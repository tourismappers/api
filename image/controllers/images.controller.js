'use strict';

//Para trabajar con imagenes
let path = require('path');
let fs = require('fs');
let HTTPStatus = require('http-status');
let utils = require('../utils/utils');

//Modelos importados
var Image = require('../schemas/image.schema');

function uploadImage(req, res) {
    let file_name = 'No subido..';
    let params = req.body;
    let image = new Image();
    if (req.files.hasOwnProperty('image')) {
        let file_path = req.files.image.path;
        // diferenciamos entre rutas en sistemas linux o windows.
        const file_split = file_path.includes('\/') ? file_path.split('\/') : file_path.split('\\');
        file_name = file_split[1];
        let ext_split = file_name.split('\.');
        let file_ext = ext_split[1];
        image.url = file_name;
        image.city = params.city;
        image.id = params.id;

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {

            image.save((err, imgSaved) => {
                if (err) {
                    res.status(200).send({ message: 'Error al subir la imagen' });
                } else {
                    if (!imgSaved) {
                        res.status(200).send({ message: 'No se ha podido actualizar el album' });
                    } else {
                        res.status(200).send(imgSaved);
                    }
                }
            });

        } else {
            res.status(200).send({ message: 'Extensión del archivo no valida' });
        }
    } else {
        res.status(200).send({ message: 'No has subido ninguna imagen...' });
    }
}

function searchImage(req, res) {
    let params = req.query;
    let city = params.city;

    Image.find({ city: city }).sort('-_id').exec((err, images) => {
        if (err) {
            utils.serverError(res);
        } else {
            if (images.length > 0) {
                utils.success(res, images)
            } else {
                utils.notFound(res);
            }
        }
    });
}

function getImage(req, res) {
    let params = req.params;
    let imageUrl = params.url;
    Image.findOne({ url: imageUrl }).exec((err, image) => {
        if (err) {
            utils.serverError(res);
        } else {
            if (image) {
                getImageFile(req, res, image);
            } else {
                utils.notFound(res);
            }
        }
    });
}
function getImageFile(req, res, image) {
    var imageFile = image.url;
    var path_file = './uploads/' + imageFile;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen...' });
        }
    });
}
function deleteImage(req, res) {
    let params = req.params;
    let imageId = params.id;
    Image.findByIdAndRemove(imageId, (err, imageRemoved) => {
        if (err) {
            utils.serverError(res, err);
        } else {
            if (imageRemoved) {
                removeFromFolder(imageRemoved.url);
                utils.success(res, imageRemoved);
            } else {
                utils.notFound(res, err);
            }
        }
    });
}
function removeFromFolder(imageUrl) {
    fs.unlink('uploads/' + imageUrl);
}


module.exports = {
    searchImage,
    uploadImage,
    getImage,
    deleteImage
}