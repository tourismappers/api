(function () {
    'use strict';
    let mongoose = require('mongoose');
    let Schema = mongoose.Schema;

    let ImageSchema = Schema({
        url: {
            type: String,
            require: true
        },
        id: {
            index: true,
            type: String,
            require: true
        },
        city: {
            index: true,
            type: String,
            require: true
        },
        uploadAt: {
            type: Date,
            require: true,
            default: new Date()
        }

    });

    module.exports = mongoose.model('Image', ImageSchema);
})(); 