'use strict';

var express = require('express');
var ImageCtrl = require('../controllers/images.controller');
var api = express.Router();
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads' });


api.post('/upload', [md_upload], ImageCtrl.uploadImage);
api.get('/search', ImageCtrl.searchImage);
api.get('/:url', ImageCtrl.getImage)
api.delete('/delete/:id', ImageCtrl.deleteImage)

module.exports = api;