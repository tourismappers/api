(function () {
    'use strict';
    let Category = require('../schemas/category.schema');
    let httpMethods = require('../utils/utils');

    function getCategories(req, res) {
        Category.find({}).sort('-_id').exec((err, categories) => {
            if (err) {
                httpMethods.serverError(res);
            } else {
                if (categories.length > 0) {
                    httpMethods.success(res, categories);
                } else {
                    httpMethods.notFound(res);
                }
            }
        })
    }

    function saveCategory(req, res) {
        let params = req.body;
        let category = new Category();
        let error = null;

        let categoryId = params._id
        let en = params.en;
        let es = params.es;
        let ca = params.ca;
        if (categoryId) {
            category._id = categoryId;
        } else {
            error = { message: 'the field categoryId is required' };
        }
        if (en) {
            category.en = en;
        } else {
            error = { message: 'the field en is required' };
        }
        if (es)
            category.es = params.es;
        if (ca)
            category.ca = params.ca;
        if (error) {
            httpMethods.serverError(res);
        } else {
            category.save((err, categoryStored) => {
                if (err) {
                    httpMethods.serverError(res, err);
                } else {
                    httpMethods.success(res, categoryStored);
                }
            })
        }
    }

    function updateCategory(req, res) {
        let categoryID = req.params.id;
        let updatedCategory = req.body;
        Category.findByIdAndUpdate(categoryID, updatedCategory, (err, updatedCategory) => {
            if (err) {
                httpMethods.serverError(res);
            } else {
                if (updatedCategory) {
                    httpMethods.success(res, updatedCategory);
                } else {
                    httpMethods.notFound(res);
                }
            }
        })
    }
    function deleteCategory(req, res) {
        let categoryID = req.params.id;
        let deletedCategory = req.body;
        Category.findByIdAndRemove(categoryID, deletedCategory, (err, deletedCategory) => {
            if (err) {
                httpMethods.serverError(res);
            } else {
                if (deletedCategory) {
                    httpMethods.success(res, deletedCategory);
                } else {
                    httpMethods.notFound(res);
                }
            }
        })
    }

    module.exports = {
        getCategories,
        saveCategory,
        updateCategory,
        deleteCategory
    }
})();