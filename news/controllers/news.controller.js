(function () {
    'use strict';
    let New = require('../schemas/new.schema');
    let Category = require('../schemas/category.schema');
    let HTTPStatus = require('http-status');
    let utils = require('../utils/utils');

    function getNews(req, res) {
        let city = req.params.city;
        let lang = req.query.lang || 'es';
        let limitEntries = req.query.entry || 12;
        New.find({ city: city, lang: lang, deleted: false }).limit(limitEntries).sort('-_id').exec((err, news) => {
            if (err) {
                utils.serverError(res);
            } else {
                if (news) {
                    utils.success(res, news);
                } else {
                    utils.notFound(res, 'noticias no encontrada');
                }
            }
        });
    }

    function getDeletedNews(req, res) {
        let city = req.params.city;
        let lang = req.query.lang || 'es';
        let limitEntries = req.query.entry || 12;
        New.find({ city: city, lang: lang, deleted: true }).limit(limitEntries).sort('-_id').exec((err, news) => {
            if (err) {
                utils.serverError(res);
            } else {
                if (news) {
                    utils.success(res, news);
                } else {
                    utils.notFound(res, 'noticias no encontrada');
                }
            }
        });
    }

    function searchNews(req, res) {
        let params = req.query;
        let limitEntries = params.view || 12;
        let categoryId = params.categoryId;
        let city = params.city;
        let lang = params.lang;
        let newId = params.newId;
        let publishedAt = params.publishedAt;
        let source = params.source;
        let published = params.published;
        let query = {};


        if (newId)
            query.newsId = newsId;
        if (city)
            query.city = city;
        if (lang)
            query.lang = lang;
        if (categoryId)
            query.categories = { $all: [{ "$elemMatch": { _id: categoryId } }] };
        if (source)
            query.source = source;
        if (publishedAt)
            query.publishedAt = publishedAt;
        if (published)
            query.published = published;

        New.find(query).limit(limitEntries).sort('-_id').exec((err, news) => {
            if (err) {
                utils.serverError(res);

            } else {
                if (news.length > 0) {
                    utils.success(res, news);
                } else {
                    res.status(HTTPStatus.NOT_FOUND).send({
                        code: HTTPStatus.NOT_FOUND,
                        message: HTTPStatus[404]
                    });
                }
            }
        });
    }
    function saveNew(req, res) {
        let params = req.body;
        let news = new New();
        let error = [];
        let city = params.city;
        news.lang = params.lang ? params.lang : 'es';
        news.published = params.published ? params.published : false;
        if (params.title) {
            news.title = params.title;
        } else {
            error.push({
                message: 'the field title is required'
            });
        }
        if (city) {
            news.city = city;
        } else {
            error.push({
                message: 'the field city is required'
            });
        }
        if (params.shortDescription)
            news.shortDescription = params.shortDescription;
        if (params.longDescription) {
            news.longDescription = params.longDescription;

        } else {
            error.push({
                message: 'the field longDescription is required'
            });
        }
        if (params.categories) {
            params.categories.map((category) => {
                news.categories.push(category);
            });
        }

        if (params.mainImgUrl)
            news.mainImgUrl = params.mainImgUrl;
        if (params.source)
            news.source = params.source;

        if (error.length > 0) {
            utils.serverError(res, error);
        } else {
            news.save((err, newStored) => {
                if (err) {
                    utils.serverError(res, err);
                } else {
                    utils.success(res, newStored);
                }
            })
        }

    }

    function updateNew(req, res) {
        let newId = req.params.id;
        let updatednews = req.body;

        New.findByIdAndUpdate(newId, updatednews, (err, newUpdated) => {
            if (err) {
                res.status(HTTPStatus.INTERNAL_SERVER_ERROR).send({
                    code: HTTPStatus.INTERNAL_SERVER_ERROR,
                    message: HTTPStatus[500]
                });
            } else {
                if (newUpdated) {
                    utils.success(res, newUpdated);
                } else {
                    res.status(HTTPStatus.NOT_FOUND).send({
                        code: HTTPStatus.NOT_FOUND,
                        message: HTTPStatus[404]
                    });
                }
            }
        });
    }

    function deleteNew(req, res) {
        let newId = req.params.id;
        New.findByIdAndRemove(newId, (err, newRemoved) => {
            if (err) {
                utils.serverError(res, err);
            } else {
                if (newRemoved) {
                    utils.success(res, newRemoved);
                } else {
                    utils.notFound(res, err);
                }
            }
        });
    }

    module.exports = {
        getNews,
        getDeletedNews,
        searchNews,
        saveNew,
        updateNew,
        deleteNew
    }
})();   