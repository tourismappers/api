(function () {
    'use strict';

    let express = require('express');
    let bodyParser = require('body-parser');
    let app = express();


    //requires routes;
    let newsRoutes = require('./routes/news.routes');
    let categoryRoutes = require('./routes/category.routes');


    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-with, Content-Type, Access-Control-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
        next();
    })

    //app.use('/tourismapps/api/v1', api);
    app.use('/tourismapps/api/v1/', newsRoutes);
    app.use('/tourismapps/api/v1/categories', categoryRoutes);


    module.exports = app;
})(); 
