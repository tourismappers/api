(function() {
    'use strict';

    let mongoose = require('mongoose');
    let app = require('./app');
    let port = process.env.PORT || 5000;
    let fs = require('fs');
    let https = require('https');
    let privateKey = fs.readFileSync('./certifications/server.key', 'utf8');
    let certificate = fs.readFileSync('./certifications/server.crt', 'utf8');
    let credentials = { key: privateKey, cert: certificate };


    mongoose.Promise = global.Promise;
    mongoose.connect('mongodb://localhost:27017/news', (err, res) => {
        if (err) {
            throw err;
        } else {
            console.log('connected with DB');
            https.createServer(credentials, app).listen(port, function() {
                console.log('listen on http://localhost:' + port);
            });

            // http No secure mode

            // app.listen(4000, function() {
            //     console.log('listen on http://localhost:' + port);
            // });

        }
    })
})();