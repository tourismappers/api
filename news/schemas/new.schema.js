(function () {
    'use strict';
    let mongoose = require('mongoose');
    let Category = require('./category.schema');
    let Schema = mongoose.Schema;


    let NewSchema = Schema({
        categories: {
            type: [Category.categorySchema],
            index: true
        },
        city: {
            type: String,
            index: true,
            require: true
        },
        deleted: {
            type: Boolean,
            require: true,
            index: true,
            default: false
        },
        published: {
            type: Boolean,
            require: true,
            default: true,
        },
        mainImgPath: {
            type: String,
        },
        mainImgUrl: {
            type: String
        },
        lang:
        {
            type: String,
            index: true,
            require: true
        },
        longDescription: {
            type: String,
            require: true
        },
        publishedAt: {
            type: Date,
            index: true,
            require: true,
            default: new Date()
        },
        shortDescription: {
            type: String
        },
        source: {
            type: String
        },
        title: {
            type: String,
            require: true
        }
    });

    module.exports = mongoose.model('New', NewSchema);
})();