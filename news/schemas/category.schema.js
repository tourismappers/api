(function () {
    'use strict';
    let mongoose = require('mongoose');
    let Schema = mongoose.Schema;

    let CategorySchema = Schema({
        _id: { type: String, index: true, require: true },
        es: String,
        en: String,
        ca: String
    });

    module.exports = mongoose.model('Category', CategorySchema);
    module.exports.categorySchema = CategorySchema;
})();