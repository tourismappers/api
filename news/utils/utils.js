(function () {
    'use strict';

    let HTTPStatus = require('http-status');

    function serverError(res, error = HTTPStatus[500]) {
        res.status(HTTPStatus.INTERNAL_SERVER_ERROR).send(error);
    }
    function notFound(res, error = HTTPStatus[404]) {
        res.status(HTTPStatus.NOT_FOUND).send(error);
    }
    function success(res, data) {
        res.status(HTTPStatus.OK).json(data);
    }


    module.exports = {
        serverError,
        notFound,
        success
    }

})();
