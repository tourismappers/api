(function () {
    'use strict';
    let express = require('express');
    let categoryCtrl = require('../controllers/category.controller');
    let api = express.Router();

    let multipart = require('connect-multiparty');
    let multipartMiddleware = multipart({ uploadDir: '../uploads' });


    api.get('/', categoryCtrl.getCategories);
    api.post('/', categoryCtrl.saveCategory);
    api.put('/:id', categoryCtrl.updateCategory);
    api.delete('/:id', categoryCtrl.deleteCategory);
    module.exports = api;
})();