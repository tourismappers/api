(function () {
    'use strict';
    let express = require('express');
    let newsCtrl = require('../controllers/news.controller');
    let api = express.Router();

    api.get('/news/:city', newsCtrl.getNews);
    api.get('/deletedNews/:city', newsCtrl.getDeletedNews);
    api.get('/search/news', newsCtrl.searchNews);
    //TODO: define API_KEY method
    api.post('/news', newsCtrl.saveNew);
    api.put('/news/:id', newsCtrl.updateNew);
    api.delete('/news/:id', newsCtrl.deleteNew);
    module.exports = api;
})();