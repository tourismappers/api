'use strict';

module.exports = function (Tag) {
    Tag.dropCity = (city, callback) => {
        Tag.destroyAll({ city })
        callback(null, `All tags in ${city} were deleted`);
    }
    Tag.remoteMethod('dropCity', {
        accepts: { arg: 'city', type: 'string' },
        returns: { arg: 'message', type: 'string' },
        http: { path: '/dropcity', verb: 'del' }
    });

};
